import pytest
from .helpers import compare_dependencies
from tests.fake_packages_data import TEST_PACKAGES_RAW, TEST_PACKAGES_HASH, TEST_PACKAGES_CIRCULAR
from src.package_installer import PackageInstaller, BadPackageFormat, CircularDependencies


@pytest.mark.parametrize("test_string,test_tuple", [
    ("KittenService: ", ("KittenService", None)),
    ("Leetmeme: Cyberportal", ("Leetmeme", ["Cyberportal"])),
    ("Cyberportal: Ice", ("Cyberportal", ["Ice"])),
    ("CamelCaser: KittenService", ("CamelCaser", ["KittenService"])),
    ("Fraudstream: Leetmeme", ("Fraudstream", ["Leetmeme"])),
    ("Ice: ", ("Ice", None)),
    ("Nine9:", ("Nine9", None)),
])
def test_string_to_tuple_happy_path(test_string, test_tuple):
    assert PackageInstaller.string_to_package_tuple(test_string) == test_tuple


@pytest.mark.parametrize("test_string", [
    "",
    "I am a bad string",
    "evEn::badder:",
    "12345678",
])
def test_string_to_tuple__raises_bad_package_exception(test_string):
    with pytest.raises(BadPackageFormat):
        PackageInstaller.string_to_package_tuple(test_string)


def test_read_package():
    # this test may be a bit overkill
    pi = PackageInstaller()
    parsed = pi.read_packages(TEST_PACKAGES_RAW)
    assert len(parsed) == len(TEST_PACKAGES_HASH)
    for name, package in TEST_PACKAGES_HASH.items():
        if name not in parsed:
            assert False
    for name, package in parsed.items():
        if name not in TEST_PACKAGES_HASH:
            assert False
        assert compare_dependencies(parsed[name].dependencies, TEST_PACKAGES_HASH[name].dependencies)


def test_get_install_order__circular_dependency_error():
    pi = PackageInstaller()
    pi.read_packages(TEST_PACKAGES_CIRCULAR)
    with pytest.raises(CircularDependencies):
        pi.get_install_order()


def test_read_packages__error_prints(capsys):
    pi = PackageInstaller()
    pi.read_packages(["Bad package format"])
    printed = capsys.readouterr()
    assert "Line not added" in printed.out


def test_get_install_order__happy_path():
    pi = PackageInstaller()
    pi.read_packages(TEST_PACKAGES_RAW)
    order = pi.get_install_order()
    assert order == 'KittenService,Ice,Cyberportal,Leetmeme,CamelCaser,Fraudstream'

