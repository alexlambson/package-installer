from src.package import Package

TEST_PACKAGES_RAW = [
    "KittenService: ",
    "Leetmeme: Cyberportal",
    "Cyberportal: Ice",
    "CamelCaser: KittenService",
    "Fraudstream: Leetmeme",
]

TEST_PACKAGES_CIRCULAR = [
    "KittenService: CamelCaser",
    "Leetmeme: Cyberportal",
    "Cyberportal: Ice",
    "CamelCaser: KittenService",
    "Fraudstream: Leetmeme",
]


TEST_PACKAGES_OBJECTS = [
    Package("KittenService"),
    Package("Leetmeme", ["Cyberportal"]),
    Package("Cyberportal", ["Ice"]),
    Package("CamelCaser", ["KittenService"]),
    Package("Fraudstream", ["Leetmeme"]),
]

TEST_PACKAGES_HASH = {
    'KittenService': Package("KittenService"),
    'Leetmeme': Package("Leetmeme", ["Cyberportal"]),
    'Cyberportal': Package("Cyberportal", ["Ice"]),
    'CamelCaser': Package("CamelCaser", ["KittenService"]),
    'Fraudstream': Package("Fraudstream", ["Leetmeme"]),
    'Ice': Package("Ice"),
}
