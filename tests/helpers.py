def compare_dependencies(list1, list2):
    if len(list1) != len(list2):
        return False
    for i, dependency1 in enumerate(list1):
        dependency2 = list2[i]
        if dependency1.package_name != dependency2.package_name:
            return False
    return True
