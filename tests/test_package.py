import pytest
from src import package
from .helpers import compare_dependencies


@pytest.mark.parametrize("name,dependency,expected", [
    ("1", ["2"], [package.Package("2")]),
    ("2", ["3"], [package.Package("3")]),
    ("4", None, [])
])
def test_package_initializes(name, dependency, expected):
    test_package = package.Package(package_name=name, dependencies=dependency)
    assert test_package.package_name == name
    assert compare_dependencies(test_package.dependencies, expected)


@pytest.mark.parametrize('name', [
    'name',
    'name1',
    'name2',
])
def test_package__add_dependency(name):
    test_package = package.Package("package name")
    test_package.add_dependency(package.Package(name))
    assert compare_dependencies(test_package.dependencies, [package.Package(name)])
