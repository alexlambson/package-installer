from typing import List


class Package:
    """
    Attributes
    ----------
    package_name : str
        Name of this package
    dependencies : list[Package]
        All the dependencies for this package. For this exercise, it will always be len=1
    """
    def __init__(self, package_name: str, dependencies: List[str] = None):
        """
        Initializes a Package with a package name

        Parameters
        ----------
        package_name : str
            name of the package
        dependencies : list(Package) optional
            will initialize :attr:`dependencies` if it is set
        """
        self.package_name = package_name
        self.dependencies = []
        if dependencies:
            self.dependencies = [Package(dependency) for dependency in dependencies if dependency]

    def add_dependency(self, dependency: "Package"):
        """
        Adds a dependency to :attr:`dependencies`

        Parameters
        ----------
        dependency : Package
            Name of dependency to add
        """
        self.dependencies.append(dependency)
