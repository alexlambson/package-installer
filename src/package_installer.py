import re

from src.package import Package
from typing import List, Dict, Tuple


class BadPackageFormat(Exception):
    pass


class CircularDependencies(Exception):
    pass


class PackageInstaller:
    """
    PackageInstaller is written with the assumption that a list of packages will be added all at once then the install
    order computed once

    Attributes
    ----------
    package_hash : dict[str, Package]
        This is where the packages will be stored after they've been parsed and added to ``PackageInstaller``
        Using a hash so I can do direct lookups ``self.package_hash[name]`` instead of a loop to search for packages
    satisfied_dependencies : list[str]
        The computed install order. Will be empty until after :meth:`PackageInstaller.get_install_order` is called
    """
    def __init__(self):
        self.package_hash = {}
        self.satisfied_dependencies = []

    def add_package(self, name: str, dependencies: List[str]=None) -> Package:
        """
        Adds a package and its dependencies to :data:`~PackageInstaller.package_hash`

        Adds dependencies by recursively calling ``add_package`` on strings in ``dependencies``.

        This function makes the assumption that once a dependency is added to the ``package_hash`` it won't be
        added again with new dependencies.

        Parameters
        ----------
        name : str
            The name of the package to be installed
        dependencies : list[str]
            A list of package names that ``name`` depends on

        Returns
        -------
        Package
            The newly created package that was added to :data:`PackageInstaller.package_hash`
        """
        if name in self.package_hash:
            # We assume that a package will only be added once.
            # Eg. Package "Hello: hi" won't be added again with different dependencies.
            package = self.package_hash[name]
        else:
            package = Package(name)

        if dependencies is None:
            dependencies = []

        for dependency in dependencies:
            dependency_package = self.add_package(dependency)
            package.add_dependency(dependency_package)

        self.package_hash[name] = package
        return package

    def get_install_order(self) -> str:
        """
        Gets the dependency order of the packages

        Returns
        -------
        str
            comma-delimited string of package names in the order they should be installed
        """
        for package in self.package_hash.values():
            self._install_order_dependencies(package, [])
        return ','.join(self.satisfied_dependencies)

    def _install_order_dependencies(self, entry_package: Package, seen: List[str]):
        """
        Recursive function that resolves the dependency order of packages in :data:`~PackageInstaller.package_hash`

        Checks for circular dependencies using the ``seen`` list

        Parameters
        ----------
        entry_package : Package
            The package to start with.
            Note that starting with a package doesn't influence where in the order it will end up.
        seen : list[str]
            The list that keeps track of what packages have already been resolved.
            If a dependency is in ``seen`` then we have a circular dependency.
            Note: This variable will be passed by reference to and modified by
            recursive calls to :meth:`~PackageInstaller._install_order_dependencies`

        Returns
        -------

        """
        seen.append(entry_package.package_name)
        for package in entry_package.dependencies:
            if package.package_name not in self.satisfied_dependencies:
                if package.package_name in seen:
                    raise CircularDependencies
                self._install_order_dependencies(package, seen)
        if entry_package.package_name not in self.satisfied_dependencies:
            self.satisfied_dependencies.append(entry_package.package_name)

    @staticmethod
    def string_to_package_tuple(package_string: str) -> Tuple[str, List[str]]:
        """
        Takes the package string format and parses it into a tuple of (name, dependencies)

        Parameters
        ----------
        package_string : str
            Expected format is ``PackageName: DependencyName``
            If there is no dependency then the format is ``PackageName: ``.
            The space after ``:`` is optional

        Returns
        -------
        (str, list[str])
            The first string is the package name, the second is the dependency package name
            The second string will be blank if there is no dependency

        Raises
        ------
        BadPackageFormat
            Raised if the package_string cannot be parsed
        """
        regex = re.match(r'^([\w\d]+)(?::)(?:\s*)([\w\d]*)$', package_string)

        if not regex:
            raise BadPackageFormat("Bad package format: ``{package_string}``".format(package_string=package_string))

        groups = regex.groups()

        if groups[1] == '':
            dependency = None
        else:
            dependency = [groups[1]]

        return groups[0], dependency

    def read_packages(self, package_list: List[str]) -> Dict[str, Package]:
        """
        Takes a list of packages and reads them into :src:`package_hash`.

        Parameters
        ----------
        package_list : list[str]
            A list of packages to pass to :func:`string_to_package_tuple`
            Format expected of each package is ``PackageName: DependencyName``

        Returns
        -------
        dict[str, Package]
        """
        for package_string in package_list:
            try:
                parsed_package_tuple = self.string_to_package_tuple(package_string)
                self.add_package(parsed_package_tuple[0], parsed_package_tuple[1])
            except BadPackageFormat as e:
                print("Line not added, reason: {}".format(str(e)))

        return self.package_hash
