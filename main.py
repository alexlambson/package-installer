from src.package_installer import PackageInstaller
from tests.fake_packages_data import TEST_PACKAGES_RAW


packages = None


def main(package_list=None):
    if not package_list:
        package_list = TEST_PACKAGES_RAW
    pi = PackageInstaller()
    pi.read_packages(package_list)
    print()
    print(pi.get_install_order())
    print()


main(packages)
